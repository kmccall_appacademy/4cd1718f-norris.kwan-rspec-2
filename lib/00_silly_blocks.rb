def reverser
  yield.split.each { |word| word.reverse! }.join(' ')
end

def adder(num = 1)
  yield + num
end

def repeater(rep = 1)
  rep.times{ yield }
end
