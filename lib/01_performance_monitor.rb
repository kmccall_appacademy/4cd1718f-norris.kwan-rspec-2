def measure(num = 1)
  start_time = Time.now
  num.times { yield }
  elapsed_time = Time.now - start_time
  average_speed = elapsed_time/num
  average_speed
end
